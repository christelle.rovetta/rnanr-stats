import subprocess
import matplotlib.pyplot as plt
plt.style.use('ggplot')

from cycler import cycler

plt.rcParams['axes.prop_cycle'] = cycler('color',
                ['royalblue', 'darkorange', 'red',
                 '#EECC55', '#88BB44', '#FFBBBB'])


estimator_color = ('royalblue', 'darkorange')
estimator_color_clt = ('royalblue', 'darkorange', 'red')
style_estimator = ['s-', 'D-']


########################################################################################################################
#                                            SEQUENCES TEST
########################################################################################################################

seq_RF00001 = "AGUAACAAUAGCAAAAGGGAUCACCUGAAUCCAUUCCGAACUCAGUAGUUAAGCCUUUUUACGCCGAAGAUACUAGAAAUAGGAAAAUAGGGAGUUACUC"
mfe_RF00001 = "((((((.........(((.....)))...(((.((((....((((((.((..((........))..))..)))).))....)))).....)))))))))."

seq_RF00001_2 = "GUCUACGGCCAUACCACCGGGAAAAAACCGGUUCUCGUCCGAUCACCGAAGUCAAGCCCGGUAGGGCCAGGUUAGUACUUGGAUGGGUGACCGCCUGGGAAUACCUGGUGCUGUAGACU"
mfe_RF00001_2 = "(((((((((....((((((((.......((((..((....))..))))........)))))).))(((((((.....((.((.(((....))))).))....))))))))))))))))."

seq_RF00174 = "CUGUUGUCGCGCAUCUUCGGUGCCCUUCGUCGGAAGGGUGAAACGGGAAUGCGGUGAGGCGAGUGAUCGCCCAAUCCGCGGCUGCCCCCGCAACUGUAAGCGGAUUGUCUUCGGUCACAUGCGCCACUGAGUUCGGCACCAGCCGCUCGGGAAGGCGAUCAGGACAACAUCCGCAAGCCAGGAGACCGGCCGAAGACCGUAGCAACGCC"
mfe_RF00174 = '.((((((.(((..(((((((.(((((((....))))))).....(((..(((((((.(((((....)))))))..)))))....)))(((...(((...((((((((((((.((((......(((.(((((..((((....)))))))))...)))))))))))))..))))))....))).....)))))))))).))).))))))..'

seq_RF00061 = "GGCGACACUCCACCAUAGAUCACUCUCCCUGUGAGGAACUACUGUCUUCACGCAGAAAGCGUCUAGCCAUGGCGUUAGUAUGAGUGUCGUGCAGCCUCCAGGACCCCCCCUCCCGGGAGAGCCAUAGUGGUCUGCGGAACCGGUGAGUACACCGGAAUUGCCAGGACGACCGGGUCCUUUCUUGGAUUAACCCGCUCAAUGCCUGGAGAUUUGGGCGUGCCCCCGCGAGAANNNUAGCCGAGUAGUGUUGGGUCGCGAAAGGCCUUGUGGUACUGCCUGAUAGGGUGCUUGCGAGUGCCCCGGGAGUNCUCGUAGACCGUGCAUCAUGAGCACAAAUCCUUAACCUCA"
mfe_RF00061 = '.((((.(((((.((.(((((((((.((..(((((((.((....)))))))))..)).((((((.......)))))).((((((...)))))).((((((.(((.......)))..)))).))...))))))))).))..((((((....)))))).(((.(((((..((.(((((((......)))....)))).))....))))).)))(((((((((((...(((((.......(((.....(((......)))....))))))))))))).))))))..(((((((....)))))))..)))))..)))).....((((.......))))...............'

seq_RF01071 = 'UACAAGAUGGGAUAGGGUGAUGCAGUAUCCUAGUCAGAGUGGCUGCUUUCGAAGGCGGGCCUAAAAAUCCGCCAAAGGGCACACCGAUGAAGUUCCUGGUUUUGGCUUCUGACGCCCAGUCAGGGGUUGAAGCUGGGAGUAAGGGAUAAGGGAGUCCUGCAAUGGCAUGCAGGAUGGAACCCUUUUCCCGUGGAGGCUAACGCACAAAAAGAAAACUAAAUUCUUUUUGUGCGUUAGUAAAACCUACCACCAGGUACAUCUAUAAUUAAGUAGAUGUGCUUGGGGUAGUGUAGCCUGCCUUGAGCGAAAUAGACGGAUGGCCGAAUUUAUGGCUCCUCUAAAAAUAGGGCCCGUUUUUAGAGGAGUCUUGAGGCCUGAAAUCUAUUUCGCAAAAGAGGCUAGAAAGCAUGCCCUCUGUCGAGGAAAGCUCCUAGGCUGUGCCGACUUUCGGCUUUACGAAGGGGAUUAAAGUGCGGACUUAGUGGCGAUCUGGUUGUGUGUUGGGCAACCGCACACCCAGACUUUUAAAGGGAAACCGCCUCCUGGCGACGGGAGGUAAGUUUGGGGGAAAUCCUACCAGACCUUAAGCCGCAAAAUUUACCCUUCGUAAAAUCACCCUAUCCCAUAAAUUU'
mfe_RF01071 = '......(((((((((((((((......((((...(((((.((((((((((...((((((........))))))...((.....)).......(((((((((((((((((((((.....)))))))))))))))))))))...((((.(((((.((((((((......))))))))....))))).))))((....((((((((((((((((((.......))))))))))))))))))...))(((((.((((((((((((((......)))))))))))))))))))..((((((..(((..((((((((((.....((((....(((.(((((((((((((((.......))))))))))))))).))))))).....))))))))))..)))))))))))))))).))))))))...)))).((((....))))..(((((...)))))(((((((((((........(((((.(((((.((...((((((.((((((.((....))))))))((((((((.....((....))((((((((....))))))))))))))))(((....))))))))))))))))))))).......))))))))))))))))))))))))))......'

dic_seq = {'RF00001': seq_RF00001, 'RF00001_2': seq_RF00001_2,
          'RF00174': seq_RF00174, 'RF00061': seq_RF00061, 'RF01071': seq_RF01071}

dic_mfe = {'RF00001': mfe_RF00001, 'RF00001_2': mfe_RF00001_2,
           'RF00174': mfe_RF00174, 'RF00061': mfe_RF00061, 'RF01071': mfe_RF01071}


########################################################################################################################
#                                        SECONDARY STRUCTURE ALGORITHMS
########################################################################################################################

def get_all_pair(bra):

    """
        Return the list of base pair associated to a secondary strucure bra
        str -> list[int]

    """

    slack = list()
    t = [-1 for k in range(len(bra))]

    for k in range(len(bra)):
        if bra[k] == "(":
            slack = [k] + slack
        if bra[k] == ")":
            t[k] = slack[0]
            t[slack[0]] = k
            slack = slack[1:]
    return t


########################################################################################################################
#                                                      VIENNA
########################################################################################################################

def structure_energy(seq: str, bra: str):

    """
        Compute the free energy

    """

    en_info = subprocess.Popen(["RNAeval"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
    (out, err) = en_info.communicate(input=seq + '\n' + bra)
    e = float(out.split("(")[-1].split(")")[0].strip())

    return e

