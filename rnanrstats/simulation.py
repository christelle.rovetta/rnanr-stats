import subprocess
import pandas as pd
import math


def sample_from_vienna(sequence: str, n_samples: int, type_sample='R'):

    """  Gets n_samples secondary structures from sequence seq using ViennaRNA package"""

    opt = ""
    if type_sample == 'NR':
        opt = "--nonRedundant"

    out_info = subprocess.Popen(["RNAsubopt", '--stochBT_en', str(n_samples), '-d2', opt],
                                stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (out, err) = out_info.communicate(input=sequence.encode())

    list_all = out.decode().split('\n')[1:-1]
    sequence = list()
    proba_structure = list()

    for line in list_all:
        list_line = line.split(' ')
        proba_structure.append(float(list_line[-1]))
        sequence.append(list_line[0])

    df_sample = pd.DataFrame({"Structure": sequence, "Proba": proba_structure})

    return df_sample


def sample_and_save(seq: str, n_samples: int, type_samples: str, folder_name: str, id_rfam=''):

    """ Sample n_samples from RNA sequence seq in a folder """

    df_sample = sample_from_vienna(seq, n_samples, type_samples)
    file_name = folder_name + "/" + id_rfam + seq[:5] + '_' + str(n_samples) + "_samples_" + type_samples

    df_sample.to_csv(file_name)

    print('Samples saved :', file_name)


def load_samples(seq: str, n_samples: int, type_samples, folder_name, id_rfam=''):

    file_name = folder_name + "/" + id_rfam + seq[:5] + '_' + str(n_samples) + "_samples_" + type_samples

    df_sample = pd.read_csv(file_name)
    df_sample = df_sample[["Structure","Proba"]]

    return df_sample


def sample_number_recommend(err: float, conf: float, img_feature=(0, 1)):

    """ Return a number of samples for an allowed error and an allowed level of confidence """

    c = (img_feature[1] - img_feature[0])**2
    d = 2 * (err ** 2)

    m = c * math.log(2/conf) / d

    return round(m) + 1


