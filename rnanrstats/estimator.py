import pandas as pd


def add_feature(df_samples: pd.DataFrame, feature, arg=list()):

    """ Add the feature defined in the function feature """

    df = pd.DataFrame({})
    df['Feature'] = df_samples['Structure'].apply(feature, arg_f=arg)
    df['Proba'] = df_samples['Proba']

    return df


def mean_estimator_r(df_sample_r: pd.DataFrame):

    """ Empirical mean """

    return df_sample_r['Feature'].mean()


def variance_estimator_r(df_sample_r: pd.DataFrame):

    """ Variance estimator """

    return df_sample_r['Feature'].var()


def mean_estimator_nr(df_sample_nr: pd.DataFrame):

    """ NR estimator for the mean """

    df_estimator = pd.DataFrame({})

    df_estimator["fp"] = df_sample_nr['Feature'] * df_sample_nr['Proba']
    df_estimator["m0"] = 1 - df_sample_nr['Proba'].cumsum() + df_sample_nr['Proba']
    df_estimator["m1"] = df_estimator['fp'].cumsum() - df_estimator['fp']
    df_estimator["est_i"] = df_sample_nr['Feature'] * df_estimator["m0"] + df_estimator["m1"]

    return df_estimator["est_i"].mean()


def variance_estimator_nr(df_sample_nr: pd.DataFrame):

    """ NR estimator for the variance """

    df_sample_nr_var = df_sample_nr.copy()
    df_sample_nr_var['Feature'] = df_sample_nr['Feature'] ** 2

    return mean_estimator_nr(df_sample_nr_var) - mean_estimator_nr(df_sample_nr)**2







